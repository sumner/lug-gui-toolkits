let
  pkgs = import <nixpkgs> {};

  # CoC Config
  cocConfig = {
    "texlab.path" = "${pkgs.texlab}/bin/texlab";
  };
in
  with pkgs;
  mkShell {
    # https://e.printstacktrace.blog/merging-json-files-recursively-in-the-command-line/
    shellHook = ''
      mkdir -p .vim
      echo '${builtins.toJSON cocConfig}' |
        ${pkgs.jq}/bin/jq -s \
          'def deepmerge(a;b):
            reduce b[] as $item (a;
              reduce ($item | keys_unsorted[]) as $key (.;
                $item[$key] as $val | ($val | type) as $type | .[$key] = if ($type == "object") then
                  deepmerge({}; [if .[$key] == null then {} else .[$key] end, $val])
                elif ($type == "array") then
                  (.[$key] + $val | unique)
                else
                  $val
                end)
              );
            deepmerge({}; .)' \
          .vim/coc-settings.json.part \
          - \
        > .vim/coc-settings.json
    '';

    buildInputs = [
      # Core
      gnumake
      neovim
      ripgrep
      rnix-lsp

      # LaTeX
      python38Packages.pygments
      texlive.combined.scheme-full

      # Python
      (
        python38.withPackages (
          ps: with ps; [
            black
            flake8
            jedi
            neovim
          ]
        )
      )
    ];
  }
