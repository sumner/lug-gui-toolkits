# GUI Toolkits
## A presentation about GUI toolkits for LUG

**Authors:**

* [Dorian Cauwe](https://gitlab.com/dcauwe)
* [Robby Zampino](https://robby.ohea.xyz)
* [Sumner Evans](https://sumnerevans.com)

[**Rendered PDF**](https://gitlab.com/sumner/lug-gui-toolkits/blob/master/gui-toolkits.pdf)
<br />
[**Flutter Slides**](https://docs.google.com/presentation/d/1ZZlt9fnRvvLlfAMnErwEBZNDmxjtPrm0qHam90eLFe8/edit?usp=sharing)

## Topics Covered

* Types of GUI toolkits

* Flutter

* The Qt Framework

  * What is Qt?
  * History of Qt?
  * The Qt Widgets Paradigm
  * The Qt Quick / QML Paradigm
  * Qt and KDE
  * Other Notes

* GTK

## Additional Resources

* [The QT Project](https://www.qt.io/)
* [Qt Widgets Documentation](https://doc.qt.io/qt-5/qtwidgets-index.html)
* [Qt Quick / QML Documentation](https://doc.qt.io/qt-5/qtqml-index.html)
